import cv2
import numpy as np


class YOLOModelHandler:
    def __init__(self, config_path, weights_path, class_names_path):
        self.net = cv2.dnn.readNetFromDarknet(config_path, weights_path)

        with open(class_names_path, 'r') as f:
            self.class_names = [line.strip() for line in f.readlines()]

        layer_names = self.net.getLayerNames()

        # Fix for handling different OpenCV versions and the output from getUnconnectedOutLayers
        try:
            self.output_layers = [layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
        except Exception:
            self.output_layers = [layer_names[i - 1] for i in self.net.getUnconnectedOutLayers()]
    def detect_objects(self, image):
        blob = cv2.dnn.blobFromImage(image, 0.00392, (416, 416), swapRB=True, crop=False)
        self.net.setInput(blob)
        layer_outputs = self.net.forward(self.output_layers)

        boxes = []
        confidences = []
        class_ids = []
        print("Layer outputs:", len(layer_outputs))

        for output in layer_outputs:
            for detection in output:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.2:
                    center_x, center_y, width, height = detection[0:4] * np.array(
                        [image.shape[1], image.shape[0], image.shape[1], image.shape[0]])
                    x = int(center_x - width / 2)
                    y = int(center_y - height / 2)

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
        print("Total detections before NMS:", len(boxes))
        print("Confidences:", confidences)
        indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.3, 0.2)

        if len(indices) > 0 and isinstance(indices, np.ndarray):
            indices = indices.flatten()
        else:
            indices = []
        print(f"Detected {len(indices)} objects")
        for i in indices:
            print(f"Confidence: {confidences[i]}")
            box = boxes[i]
            x, y, w, h = box
            label = str(self.class_names[class_ids[i]])
            confidence = str(round(confidences[i], 2))
            color = [int(c) for c in np.random.randint(0, 255, size=3)]
            cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
            cv2.putText(image, f"{label}: {confidence}", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        return image  # Return the image with drawn detections

