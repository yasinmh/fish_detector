import os
import cv2
from yolo_integration.yolo_model_handler import YOLOModelHandler  # Assuming this is correctly implemented
from preprocessing.denoising import ImageDenoiser

# Initialization (unchanged)
current_directory = os.path.dirname(os.path.abspath(__file__))
video_path = os.path.join(current_directory, 'resources', 'videos', 'test_videos', 'video')
yolo_cfg_path = os.path.join(current_directory, 'resources', 'yolo', 'yolov4-tiny.cfg')
yolo_weights_path = os.path.join(current_directory, 'resources', 'yolo', 'yolov4-tiny_best.weights')
obj_names_path = os.path.join(current_directory, 'resources', 'yolo', 'obj.names')

yolo_model = YOLOModelHandler(yolo_cfg_path, yolo_weights_path, obj_names_path)
image_denoiser = ImageDenoiser(h=10, template_window_size=7, search_window_size=21)

def process_frame(frame, frame_count, process_every_n_frames=2):
    # Process every Nth frame for speed
    if frame_count % process_every_n_frames != 0:
        return frame

    # Object detection using YOLO (adjust as needed if YOLO expects full resolution)
    detections = yolo_model.detect_objects(frame)

    return detections  # Assuming detections are drawn on the frame

def main():
    for i in range(43):
        video = f"{video_path}/{i +1}.mp4"
        cap = cv2.VideoCapture(video)
        frame_count = 0
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            processed_frame = yolo_model.detect_objects(frame)
            cv2.imshow('Processed Frame', processed_frame)

            frame_count += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
