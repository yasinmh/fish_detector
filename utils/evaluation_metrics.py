import cv2


def generate_region_proposals(image, threshold=127):
    """
    Generate region proposals from an image using simple thresholding.

    Parameters:
    - image: The input image from which to generate proposals.
    - threshold: The threshold value for binary segmentation.

    Returns:
    - proposals: List of proposed regions, each defined by [x, y, width, height].
    """
    # Convert the image to grayscale
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply thresholding to segment the image
    _, binary_image = cv2.threshold(gray_image, threshold, 255, cv2.THRESH_BINARY)

    # Find contours from the binary image
    contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Generate bounding boxes from contours
    proposals = [cv2.boundingRect(contour) for contour in contours]

    return proposals
